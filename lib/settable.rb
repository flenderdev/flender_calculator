module Settable
  def set(key, value)
    method = "set_#{key.to_s}".to_sym
    if self.respond_to?(method, true)
      send(method, value)
    else
      raise NoMethodError.new
    end
    self
  end

  def set_hash(hash)
    hash.to_a.each do |key, value|
      set(key, value)
    end
  end
end