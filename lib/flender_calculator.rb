require "flender_calculator/version"
require 'flender_calculator/apr'
require 'flender_calculator/floatable'
require 'flender_calculator/pmt'
require 'flender_calculator/rate'
require 'flender_calculator/total'
require 'flender_calculator/loan'
require 'flender_calculator/mca_loan'
