require 'settable.rb'

module FlenderCalculator
  class InvalidValue < StandardError
  end

  class InvalidDiscount < StandardError
    def message
      "Discount amount can't exceed total fee amount"
    end
  end

  class Loan
    include Settable

    attr_reader :amount, :term, :interest_rate, :fee, :arrangement_fee_percent, :adjustment_values,
                :arrangement_fee, :arrangement_fee_amount, :management_fee, :total_fee, :total_fee_amount, :management_fee_amount, :fees_amortization_period,
                :monthly_payment, :management_fee_regular_payment, :monthly_payment_with_fee, :total_service_fee_amount,
                :repayment_schedule, :net_drawdown, :discount_percent, :discount_amount, :total_fee_without_discount,
                :discount, :servicing_fee_percent, :servicing_fee_amount, :monthly_payment_with_service_fee,
                :max_discount_amount

    DEFAULT_ADJUSTMENT_VALUES = [
      {from: 0, to: 50_000, value: 1.5},
      {from: 50_000, to: 75_000, value: 1.1},
      {from: 75_000, to: 100_000, value: 1.05},
      {from: 100_000, to: 150_000, value: 1},
      {from: 150_000, to: 200_000, value: 0.95},
      {from: 200_000, to: 250_000, value: 0.9},
      {from: 250_000, to: 1_000_000, value: 0.75},
    ]

    def initialize(amount, term, rate, fee)
      initialize_defaults
      set_amount(amount)
      set_term(term)
      set_interest_rate(rate)
      set_fee(fee)
    end

    def calculate
      calculate_fees
      calculate_pmt
      calculate_servicing_fee_amount
      calculate_net_drawdown
      calculate_repayment_schedule
    end

    protected

    def set_amount(amount)
      self.amount = amount
    end

    def set_term(term)
      self.term = term
    end

    def set_interest_rate(value)
      self.interest_rate = value
    end

    def set_fee(value)
      self.fee = value
    end

    def set_discount_amount(value)
      raise InvalidValue.new if value.to_f < 0

      self.discount_amount = value.to_f
    end

    def initialize_defaults
      set_adjustment_values(DEFAULT_ADJUSTMENT_VALUES)
      set_arrangement_fee_percent(1)
      set_fees_amortization_period(12)
      set_servicing_fee_percent(0)
      self.discount_percent = 0
      self.discount_amount  = 0
      self.discount  = 0
    end

    def set_arrangement_fee_percent(value)
      self.arrangement_fee_percent = value
    end

    def set_fees_amortization_period(value)
      self.fees_amortization_period = value
    end

    def set_adjustment_values(values)
      self.adjustment_values = values
    end

    def set_servicing_fee_percent(value)
      self.servicing_fee_percent = value
    end

    def get_adjustment_value(amount = nil)
      adj_values = DEFAULT_ADJUSTMENT_VALUES
      unless self.adjustment_values.nil? || self.adjustment_values.empty?
        adj_values = self.adjustment_values
      end

      selected_value = adj_values.select{|adj| adj[:from].to_f <= amount.to_f && amount.to_f < adj[:to].to_f }
      return 1 if selected_value.empty?

      selected_value.first[:value].to_f.round(2)
    end

    def calculate_total_fee
      self.total_fee_without_discount = self.fee.to_f * get_adjustment_value(self.amount)
      calculate_discount
      self.total_fee = self.total_fee_without_discount - self.discount_percent.to_f
      raise InvalidDiscount.new if self.total_fee < 0
    end

    def calculate_fees
      calculate_total_fee
      calculate_arrangement_fee
      calculate_management_fee
      calculate_total_fee_amount
      calculate_max_discount_amount
    end

    def calculate_discount
      self.discount_percent = (self.discount_amount.to_f * 100) / self.amount.to_f
      self.discount = (self.discount_percent * 100) / self.total_fee_without_discount.to_f
    end

    def calculate_arrangement_fee
      self.arrangement_fee = self.total_fee.to_f * self.arrangement_fee_percent.to_f
      self.arrangement_fee_amount = (self.amount.to_f * self.arrangement_fee.to_f / 100)
    end

    def calculate_total_fee_amount
      self.total_fee_amount = self.arrangement_fee_amount + self.management_fee_amount
    end

    def calculate_max_discount_amount
      self.max_discount_amount = self.arrangement_fee_amount.to_f + self.discount_amount.to_f
    end

    def calculate_management_fee
      self.management_fee = self.total_fee.to_f - self.arrangement_fee.to_f
      calculate_management_fees_amount
    end

    def calculate_servicing_fee_amount
      self.servicing_fee_amount = ((self.monthly_payment.to_f * self.servicing_fee_percent.to_f) / 100).to_f.round(2)
      calculate_pmt_with_service_fee
      calculate_total_service_fee_amount
    end

    def calculate_management_fees_amount
      self.management_fee_amount = self.management_fee.to_f != 0 ? self.amount * self.management_fee / 100  : 0
      calculate_management_fee_regular_payment
    end

    def calculate_management_fee_regular_payment
      self.management_fee_regular_payment = (self.management_fee_amount.to_f / self.fees_amortization_period.to_i).to_f.round(2)
    end

    def calculate_pmt
      self.monthly_payment = Pmt.calc(self.interest_rate.to_f/100.0, self.term.to_i, self.amount.to_i)
      calculate_pmt_with_fee
    end

    def calculate_pmt_with_fee
      self.monthly_payment_with_fee = self.monthly_payment.to_f + self.management_fee_regular_payment.to_f
    end

    def calculate_pmt_with_service_fee
      self.monthly_payment_with_service_fee = (self.monthly_payment.to_f + self.servicing_fee_amount.to_f).round(2)
    end

    def calculate_total_service_fee_amount
      self.total_service_fee_amount = (self.servicing_fee_amount.to_f * self.term.to_f).round(2)
    end

    def calculate_repayment_schedule
      self.repayment_schedule = {}
      1.upto(self.term.to_i).each do |period|
        pmt = self.monthly_payment
        fee = period <= self.fees_amortization_period.to_i ? self.management_fee_regular_payment : 0
        self.repayment_schedule[period] = {
          loan_payment: pmt,
          fee_payment: fee,
          total_payment: pmt + fee,
          cashflow: pmt + fee,
        }
      end
    end

    def calculate_net_drawdown
      self.net_drawdown = self.amount.to_i - self.arrangement_fee_amount.to_f
    end

    attr_writer :amount, :term, :grade, :fee, :interest_rate, :arrangement_fee_percent, :adjustment_values,
                :arrangement_fee, :arrangement_fee_amount, :management_fee, :total_fee, :total_fee_amount, :management_fee_amount, :fees_amortization_period,
                :monthly_payment, :management_fee_regular_payment, :monthly_payment_with_fee, :total_service_fee_amount,
                :repayment_schedule, :net_drawdown, :discount_percent, :discount_amount, :total_fee_without_discount,
                :discount, :servicing_fee_percent, :servicing_fee_amount, :monthly_payment_with_service_fee,
                :max_discount_amount
  end

  class BrokerLoan < Loan
    DEFAULT_BROKER_BOTTOM_LINE = 1.25
    DEFAULT_BROKER_FEE = 1

    attr_reader :broker_fee, :broker_fee_amount, :broker_bottom_line

    def initialize(amount, term, rate, fee)
      super(amount, term, rate, fee)
      set_broker_fee(DEFAULT_BROKER_FEE)
      set_broker_bottom_line(DEFAULT_BROKER_BOTTOM_LINE)
    end

    protected

    def set_broker_fee(value)
      raise InvalidValue.new if value < 0

      self.broker_fee = value.to_f
    end

    def set_broker_bottom_line(value)
      raise InvalidValue.new if value < 0

      self.broker_bottom_line = value
    end

    def calculate_total_fee
      self.total_fee_without_discount = self.fee.to_f * get_adjustment_value(self.amount)
      if self.broker_fee.to_f < self.broker_bottom_line.to_f
        self.total_fee_without_discount -= (self.broker_bottom_line - self.broker_fee.to_f)
        self.total_fee_without_discount = 0 if self.total_fee_without_discount < 0
      end
      calculate_discount
      self.total_fee = self.total_fee_without_discount - self.discount_percent.to_f
      raise InvalidDiscount.new if self.total_fee < 0
    end

    def calculate_fees
      calculate_total_fee
      calculate_broker_fee
      calculate_arrangement_fee
      calculate_management_fee
      calculate_total_fee_amount
      calculate_net_drawdown
      calculate_max_discount_amount
    end

    def calculate_broker_fee
      self.broker_fee_amount = (self.amount.to_f * self.broker_fee.to_f / 100)
    end

    def calculate_total_fee_amount
      self.total_fee_amount = self.arrangement_fee_amount + self.management_fee_amount + self.broker_fee_amount
    end

    def calculate_net_drawdown
      self.net_drawdown = self.amount.to_i - self.arrangement_fee_amount.to_f - self.broker_fee_amount.to_f
    end

    attr_writer :broker_fee, :broker_fee_amount, :broker_bottom_line
  end
end