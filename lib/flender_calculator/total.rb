class FlenderCalculator::Total
  class << self
    def calc month_rate, month_count, total_sum
      month_count * FlenderCalculator::Pmt.calc(month_rate, month_count, total_sum)
    end
  end
end