class FlenderCalculator::Rate
  class InvalidValue < StandardError
  end

  class << self
    def calc month_count, pmt, total_sum
      raise InvalidValue.new if month_count == 0 || (pmt == 0 && total_sum == 0)

      Exonio.rate(month_count, pmt, -total_sum) * 12
    end
  end
end