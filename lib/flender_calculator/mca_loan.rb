# frozen_string_literal: true

require 'settable.rb'

module FlenderCalculator
  class McaLoan
    include Settable
    BASE_TERM = 1
    BASE_BORROWER_RATE = 1.07
    BASE_LENDER_RATE = 1.06

    attr_accessor :lender_rate, :borrower_rate, :amount, :total_repayment_amount,
                  :factoring_fee, :arrangement_fee, :arrangement_fee_values, :arrangement_fee_percent,
                  :lender_return, :borrower_return, :total_sum, :daily_average, :weekly_average,
                  :annual_average, :term, :turnover, :flender_net_return, :fire_costs, :monthly_average,
                  :daily_repayment_amount, :split_level, :flender_net_return_percent, :broker_fee, :broker_fee_percent,
                  :margin_rate, :margin_fee

    DEFAULT_ARRANGEMENT_FEE_VALUES = [
      { amount_from: 0,      amount_to: 20_000,   value: 2000, fire_cost: 400 },
      { amount_from: 20_001, amount_to: 30_000,   value: 2500, fire_cost: 500 },
      { amount_from: 30_001, amount_to: 50_000,   value: 3500, fire_cost: 850 },
      { amount_from: 50_001, amount_to: 75_000,   value: 4500, fire_cost: 1000 },
      { amount_from: 75_001, amount_to: 100_000,  value: 5500, fire_cost: 1200 },
      { amount_from: 100_001, amount_to: 1_000_000, value: 5500, fire_cost: 2200 }
    ].freeze

    def initialize(amount, turnover)
      @amount = amount
      @term = BASE_TERM
      @lender_rate = BASE_LENDER_RATE
      @borrower_rate = BASE_BORROWER_RATE
      @turnover = turnover
      @monthly_average = turnover
      @arrangement_fee_values = DEFAULT_ARRANGEMENT_FEE_VALUES
    end

    def calculate
      run_checks
      calculate_arrangement_fees
      calculate_total_sum
      calculate_factoring_fees
      calculate_margin_rate
      calculate_margin_fee
      calculate_total_repayment_amount
      calculate_averages
      calculate_fire_costs
      calculate_flender_net_return
      calculate_flender_net_return_percent
      calculate_lender_return
      calculate_broker_fee_percent
      calculate_broker_fee
      calculate_arrangement_fee_percent
      round_all_values
      self
    end

    private

    def round_all_values
      instance_variables.each do |variable|
        value = instance_variable_get(variable)
        next unless value.respond_to?(:to_f)
        next if %w[borrower_rate lender_rate margin_rate].include?(variable[1..-1])

        instance_variable_set(variable, value.to_f.round(2))
      end
      instance_variable_set(:@borrower_rate, @borrower_rate.to_f.round(3))
      instance_variable_set(:@lender_rate, @lender_rate.to_f.round(3))
      instance_variable_set(:@margin_rate, @margin_rate.to_f.round(3))
    end

    def set_arrangement_fee_values(value)
      @arrangement_fee_values = value
    end

    def set_arrangement_fee(value)
      raise 'Invalid attribute value' unless value.is_a?(Numeric)

      @arrangement_fee = value.to_f
    end

    def set_term(value)
      raise 'Invalid attribute value' unless value.is_a?(Numeric)

      @term = value.to_i
    end

    def set_lender_rate(value)
      raise 'Invalid attribute value' unless value.is_a?(Numeric)

      @lender_rate = value.to_f
    end

    def set_borrower_rate(value)
      raise 'Invalid attribute value' unless value.is_a?(Numeric)

      @borrower_rate = value.to_f
    end

    def set_split_level(value)
      raise 'Invalid attribute value' unless value.is_a?(Numeric)

      @split_level = value.to_f
    end

    def set_daily_repayment_amount(value)
      raise 'Invalid attribute value' unless value.is_a?(Numeric)

      @daily_repayment_amount = value.to_f
    end

    def set_margin_rate(value)
      raise 'Invalid attribute value' unless value.is_a?(Numeric)

      @margin_rate = value.to_f
    end

    def set_broker_fee_percent(value)
      @broker_fee_percent = value.to_f
    end

    def run_checks
      raise 'Invalid Attributes' unless verify_presence
    end

    def verify_presence
      %i[amount lender_rate turnover term].all? do |key|
        variable = instance_variable_get("@#{key}")
        variable.is_a?(Numeric) && variable >= 0
      end
    end

    def calculate_broker_fee
      @broker_fee = (amount * broker_fee_percent.to_f) / 100
    end

    def calculate_broker_fee_percent
      @broker_fee_percent ||= 0
    end

    def calculate_arrangement_fees
      @arrangement_fee ||= arrangement_fee_hash[:value] || 0
    end

    def calculate_arrangement_fee_percent
      @arrangement_fee_percent = (arrangement_fee * 100) / amount.to_f
    end

    def calculate_factoring_fees
      @factoring_fee = borrower_rate <= 1 ? 0 : ((total_sum * borrower_rate) - total_sum)
    end

    def calculate_margin_fee
      @margin_fee = total_sum * margin_rate.to_f
    end

    def calculate_margin_rate
      @margin_rate ||= borrower_rate.to_f - lender_rate.to_f
    end

    def calculate_total_repayment_amount
      @total_repayment_amount = total_sum.to_f + factoring_fee.to_f + margin_fee.to_f
    end

    def calculate_total_sum
      @total_sum = amount + arrangement_fee.to_f + broker_fee.to_f
    end

    def calculate_averages
      calculate_annual_average
      calculate_daily_average
      calculate_weekly_average
    end

    def calculate_annual_average
      @annual_average = turnover.to_f * 12
    end

    def calculate_daily_average
      @daily_average = annual_average.to_f / 240
    end

    def calculate_weekly_average
      @weekly_average = annual_average.to_f / 52
    end

    def calculate_lender_return
      @lender_return = amount.to_f * lender_rate.to_f
    end

    def calculate_flender_net_return
      @flender_net_return = (margin_fee.to_f + arrangement_fee.to_f) - fire_costs.to_f
    end

    def calculate_flender_net_return_percent
      @flender_net_return_percent = (flender_net_return.to_f / amount.to_f) * 100
    end

    def calculate_fire_costs
      @fire_costs = arrangement_fee_hash[:fire_cost] || 0
    end

    def arrangement_fee_hash
      @arrangement_fee_hash ||= arrangement_fee_values.find do |obj|
        (obj[:amount_from].to_f <= amount) && (amount <= obj[:amount_to].to_f)
      end
    end
  end
end
