require "test_helper"

class FlenderCalculatorLoanTest < Minitest::Test
  def setup
    @amount = 100_000
    @term = 36
    @fee = 4
    @rate = 6.45
    @discount_amount = 2_000
    @broker_fee = 1
  end

  def test_calculations
    loan_calculator = FlenderCalculator::Loan.new(@amount, @term, @rate, @fee)
    assert_equal loan_calculator.amount, @amount
    assert_equal loan_calculator.term, @term
    assert_equal loan_calculator.interest_rate, @rate
    assert_equal loan_calculator.fee, @fee
    loan_calculator.calculate
    assert_equal loan_calculator.net_drawdown, 96_000
    assert_equal loan_calculator.total_fee_amount, 4000
    assert_equal loan_calculator.monthly_payment.to_f.round(2), 3062.63
    assert_equal loan_calculator.management_fee_regular_payment.to_f.round(2), 0.0
    assert_equal loan_calculator.repayment_schedule.size, @term.to_i
    assert_equal loan_calculator.repayment_schedule[@term][:fee_payment], 0

    loan_calculator.set(:amount, 300_000).calculate
    assert_equal loan_calculator.net_drawdown, 291_000
    assert_equal loan_calculator.total_fee_amount, 9000
    assert_equal loan_calculator.arrangement_fee_amount, 9000
    assert_equal loan_calculator.monthly_payment.to_f.round(2), 9187.88
    assert_equal loan_calculator.management_fee_regular_payment.to_f.round(2), 0.0

    loan_calculator.set(:arrangement_fee_percent, 0.5).calculate
    assert_equal loan_calculator.net_drawdown, 295_500
    assert_equal loan_calculator.total_fee_amount, 9000
    assert_equal loan_calculator.management_fee_amount, 4500
    assert_equal loan_calculator.arrangement_fee_amount, 4500
    assert_equal loan_calculator.monthly_payment.to_f.round(2), 9187.88
    assert_equal loan_calculator.management_fee_regular_payment.to_f.round(2), 375.0

    loan_calculator.set(:servicing_fee_percent, 0.2).calculate
    assert_equal loan_calculator.servicing_fee_amount, 18.38
    assert_equal loan_calculator.servicing_fee_percent, 0.2
    assert_equal loan_calculator.total_service_fee_amount, 661.68
    assert_equal loan_calculator.total_fee_amount, 9000
    assert_equal loan_calculator.monthly_payment_with_service_fee.to_f.round(2), 9206.26

    loan_calculator.set(:servicing_fee_percent, 0).calculate
    assert_equal loan_calculator.total_fee_amount, 9000
    assert_equal loan_calculator.servicing_fee_amount, 0
    assert_equal loan_calculator.total_service_fee_amount, 0
    assert_equal loan_calculator.monthly_payment_with_service_fee.to_f.round(2), 9187.88
  end

  def test_calculations_with_discount
    loan_calculator = FlenderCalculator::Loan.new(@amount, @term, @rate, @fee)
    loan_calculator.set(:discount_amount, @discount_amount)
    assert_equal loan_calculator.amount, @amount
    assert_equal loan_calculator.term, @term
    assert_equal loan_calculator.interest_rate, @rate
    assert_equal loan_calculator.fee, @fee
    assert_equal loan_calculator.discount, 0
    assert_equal loan_calculator.discount_amount, @discount_amount
    assert_equal loan_calculator.discount_percent, 0
    loan_calculator.calculate
    assert_equal loan_calculator.discount_percent, 2
    assert_equal loan_calculator.net_drawdown, 98_000
    assert_equal loan_calculator.total_fee_amount, 2_000
    assert_equal loan_calculator.discount, 50.0
    assert_equal loan_calculator.discount_amount, 2_000
    assert_equal loan_calculator.max_discount_amount, 4_000
    assert_equal loan_calculator.monthly_payment.to_f.round(2), 3062.63
    assert_equal loan_calculator.management_fee_regular_payment.to_f.round(2), 0.0
    assert_equal loan_calculator.repayment_schedule.size, @term.to_i
    assert_equal loan_calculator.repayment_schedule[@term][:fee_payment], 0

    loan_calculator.set(:amount, 300_000).set(:discount_amount, 4_500).calculate
    assert_equal loan_calculator.net_drawdown, 295_500
    assert_equal loan_calculator.total_fee_amount, 4_500
    assert_equal loan_calculator.discount_percent, 1.5
    assert_equal loan_calculator.discount, 50.0
    assert_equal loan_calculator.max_discount_amount, 9_000.0
    assert_equal loan_calculator.arrangement_fee_amount, 4_500
    assert_equal loan_calculator.monthly_payment.to_f.round(2), 9187.88
    assert_equal loan_calculator.management_fee_regular_payment.to_f.round(2), 0.0

    loan_calculator.set(:arrangement_fee_percent, 0.5).calculate
    assert_equal loan_calculator.net_drawdown, 297_750
    assert_equal loan_calculator.total_fee_amount, 4_500
    assert_equal loan_calculator.discount_amount, 4_500
    assert_equal loan_calculator.max_discount_amount, 6_750
    assert_equal loan_calculator.management_fee_amount, 2_250
    assert_equal loan_calculator.arrangement_fee_amount, 2_250
    assert_equal loan_calculator.monthly_payment.to_f.round(2), 9187.88
    assert_equal loan_calculator.management_fee_regular_payment.to_f.round(2), 187.5

    loan_calculator.set(:amount, 300_000).set(:discount_amount, 9_000).set(:arrangement_fee_percent, 0).calculate
    assert_equal loan_calculator.net_drawdown, 300_000
    assert_equal loan_calculator.total_fee_amount, 0
    assert_equal loan_calculator.discount, 100.0
    assert_equal loan_calculator.max_discount_amount, 9_000.0
    assert_equal loan_calculator.discount_percent, 3.0
    assert_equal loan_calculator.arrangement_fee_amount, 0
    assert_equal loan_calculator.monthly_payment.to_f.round(2), 9187.88
    assert_equal loan_calculator.management_fee_regular_payment.to_f.round(2), 0.0
  end

  def test_that_it_gets_adjustment_values
    loan_calculator = FlenderCalculator::BrokerLoan.new(@amount, @term, @rate, @fee)
    loan_calculator.send(:set_adjustment_values, [])
    assert_equal loan_calculator.send(:get_adjustment_value), 1.5
    loan_calculator.send(:set_adjustment_values, nil)
    assert_equal loan_calculator.send(:get_adjustment_value), 1.5
  end

  def test_broker_calculator
    loan_calculator = FlenderCalculator::BrokerLoan.new(@amount, @term, @rate, @fee)
    loan_calculator.set(:broker_fee, @broker_fee)
    assert_equal loan_calculator.amount, @amount
    assert_equal loan_calculator.term, @term
    assert_equal loan_calculator.interest_rate, @rate
    assert_equal loan_calculator.fee, @fee
    assert_equal loan_calculator.broker_fee, @broker_fee
    loan_calculator.calculate
    assert_equal loan_calculator.net_drawdown, 95_250
    assert_equal loan_calculator.total_fee_amount, 4_750
    assert_equal loan_calculator.broker_fee_amount, 1_000
    assert_equal loan_calculator.monthly_payment.to_f.round(2), 3_062.63
    assert_equal loan_calculator.management_fee_regular_payment.to_f.round(2), 0.0
    assert_equal loan_calculator.repayment_schedule.size, @term.to_i
    assert_equal loan_calculator.repayment_schedule[@term][:fee_payment], 0

    loan_calculator.set(:amount, 300_000).calculate
    assert_equal loan_calculator.net_drawdown,   288_750.00
    assert_equal loan_calculator.total_fee_amount, 11_250
    assert_equal loan_calculator.arrangement_fee_amount, 8250
    assert_equal loan_calculator.broker_fee_amount, 3000
    assert_equal loan_calculator.monthly_payment.to_f.round(2), 9187.88
    assert_equal loan_calculator.management_fee_regular_payment.to_f.round(2), 0.0

    loan_calculator.set(:fee, 0).calculate
    assert_equal loan_calculator.net_drawdown, 297_000.00
    assert_equal loan_calculator.total_fee_amount, 3_000
    assert_equal loan_calculator.arrangement_fee_amount, 0
    assert_equal loan_calculator.broker_fee_amount, 3_000
    assert_equal loan_calculator.monthly_payment.to_f.round(2), 9187.88
    assert_equal loan_calculator.management_fee_regular_payment.to_f.round(2), 0.0
  end

  def test_broker_calculator_with_discount
    loan_calculator = FlenderCalculator::BrokerLoan.new(@amount, @term, @rate, @fee)
    loan_calculator.set(:broker_fee, @broker_fee)
    loan_calculator.calculate
    broker_fee_amount_without_discount = loan_calculator.broker_fee_amount

    loan_calculator.set(:discount_amount, 1_875)
    assert_equal loan_calculator.amount, @amount
    assert_equal loan_calculator.term, @term
    assert_equal loan_calculator.interest_rate, @rate
    assert_equal loan_calculator.fee, @fee
    assert_equal loan_calculator.broker_fee, @broker_fee
    assert_equal loan_calculator.discount_percent, 0
    loan_calculator.calculate
    assert_equal loan_calculator.discount_percent, 1.875
    assert_equal loan_calculator.net_drawdown, 97_125
    assert_equal loan_calculator.total_fee_amount, 2_875
    assert_equal loan_calculator.broker_fee_amount, broker_fee_amount_without_discount
    assert_equal loan_calculator.discount, 50.0
    assert_equal loan_calculator.max_discount_amount, 3_750.0
    assert_equal loan_calculator.monthly_payment.to_f.round(2), 3_062.63
    assert_equal loan_calculator.management_fee_regular_payment.to_f.round(2), 0.0
    assert_equal loan_calculator.repayment_schedule.size, @term.to_i
    assert_equal loan_calculator.repayment_schedule[@term][:fee_payment], 0

    loan_calculator.set(:amount, 300_000).calculate
    broker_fee_amount_without_discount = loan_calculator.broker_fee_amount
    loan_calculator.set(:discount_amount, 4_125).calculate
    assert_equal loan_calculator.net_drawdown, 292_875.00
    assert_equal loan_calculator.total_fee_amount, 7_125
    assert_equal loan_calculator.arrangement_fee_amount, 4_125
    assert_equal loan_calculator.broker_fee_amount, broker_fee_amount_without_discount
    assert_equal loan_calculator.discount_percent, 1.375
    assert_equal loan_calculator.discount, 50.0
    assert_equal loan_calculator.max_discount_amount, 8_250.0
    assert_equal loan_calculator.monthly_payment.to_f.round(2), 9187.88
    assert_equal loan_calculator.management_fee_regular_payment.to_f.round(2), 0.0

    loan_calculator.set(:discount_amount, 8_250).calculate
    assert_equal loan_calculator.net_drawdown,   297_000.00
    assert_equal loan_calculator.total_fee_amount, broker_fee_amount_without_discount
    assert_equal loan_calculator.arrangement_fee_amount, 0
    assert_equal loan_calculator.broker_fee_amount, broker_fee_amount_without_discount
    assert_equal loan_calculator.discount_percent, 2.75
    assert_equal loan_calculator.discount, 100.0
    assert_equal loan_calculator.max_discount_amount, 8_250.0
    assert_equal loan_calculator.monthly_payment.to_f.round(2), 9187.88
    assert_equal loan_calculator.management_fee_regular_payment.to_f.round(2), 0.0

    loan_calculator.set(:broker_fee, 2).calculate
    broker_fee_amount_without_discount = loan_calculator.broker_fee_amount
    loan_calculator.set(:discount_amount, 9_000).calculate
    assert_equal loan_calculator.net_drawdown,   294_000.00
    assert_equal loan_calculator.total_fee_amount, broker_fee_amount_without_discount
    assert_equal loan_calculator.arrangement_fee_amount, 0
    assert_equal loan_calculator.broker_fee_amount, broker_fee_amount_without_discount
    assert_equal loan_calculator.discount_percent, 3.0
    assert_equal loan_calculator.discount, 100
    assert_equal loan_calculator.max_discount_amount, 9_000
    assert_equal loan_calculator.monthly_payment.to_f.round(2), 9187.88
    assert_equal loan_calculator.management_fee_regular_payment.to_f.round(2), 0.0
  end

  def test_set_discount_amount
    loan_calculator = FlenderCalculator::Loan.new(@amount, @term, @rate, @fee)
    assert_raises FlenderCalculator::InvalidDiscount do
      loan_calculator.set(:discount_amount, 5_000).calculate
    end
    assert_raises FlenderCalculator::InvalidValue do
      loan_calculator.set(:discount_amount, -1)
    end
    loan_calculator.set(:discount_amount, @discount_amount)
    assert_equal loan_calculator.discount_amount, @discount_amount
  end

  def test_set_discount_amount_broker
    loan_calculator = FlenderCalculator::BrokerLoan.new(@amount, @term, @rate, @fee)
    assert_raises FlenderCalculator::InvalidDiscount do
      loan_calculator.set(:discount_amount, 5_000).calculate
    end
    assert_raises FlenderCalculator::InvalidValue do
      loan_calculator.set(:discount_amount, -1)
    end
    loan_calculator.set(:discount_amount, @discount_amount)
    assert_equal loan_calculator.discount_amount, @discount_amount
  end
end