require 'test_helper'

class FlenderCalculatorMcaLoanTest < Minitest::Test
  def test_calculations
    mca_loan_calculator = FlenderCalculator::McaLoan.new(50_000, 123_000)
    assert_equal mca_loan_calculator.lender_rate, FlenderCalculator::McaLoan::BASE_LENDER_RATE
    assert_equal mca_loan_calculator.borrower_rate, FlenderCalculator::McaLoan::BASE_BORROWER_RATE
    assert_equal mca_loan_calculator.term, FlenderCalculator::McaLoan::BASE_TERM

    mca_loan_calculator.set(:lender_rate, 1.11).set(:borrower_rate, 1.12).set(:term, 12)
    assert_equal mca_loan_calculator.lender_rate, 1.11
    assert_equal mca_loan_calculator.borrower_rate, 1.12
    assert_equal mca_loan_calculator.term, 12

    mca_loan_calculator.calculate
    assert_equal mca_loan_calculator.amount, 50_000
    assert_equal mca_loan_calculator.lender_rate, 1.11
    assert_equal mca_loan_calculator.borrower_rate, 1.12
    assert_equal mca_loan_calculator.arrangement_fee_values,
                 FlenderCalculator::McaLoan::DEFAULT_ARRANGEMENT_FEE_VALUES

    mca_loan_calculator.calculate
    assert_equal mca_loan_calculator.arrangement_fee, 3_500
    assert_equal mca_loan_calculator.total_sum, 53_500
    assert_equal mca_loan_calculator.factoring_fee, 6420
    assert_equal mca_loan_calculator.margin_fee, 535
    assert_equal mca_loan_calculator.total_repayment_amount, 60455

    assert_raises RuntimeError do
      FlenderCalculator::McaLoan.new('test', 'test').calculate
    end

    assert_raises RuntimeError do
      FlenderCalculator::McaLoan.new(123, 123).set(:term, nil).calculate
    end

    assert_raises RuntimeError do
      FlenderCalculator::McaLoan.new(123, 123).set(:lender_rate, nil).calculate
    end

    assert_raises RuntimeError do
      FlenderCalculator::McaLoan.new(123, 123).set(:borrower_rate, nil).calculate
    end

    assert_raises RuntimeError do
      FlenderCalculator::McaLoan.new(123, 123).set(:term, 'test').calculate
    end

    assert_raises RuntimeError do
      FlenderCalculator::McaLoan.new(123, 123).set(:lender_rate, 'test').calculate
    end

    assert_raises RuntimeError do
      FlenderCalculator::McaLoan.new(123, 123).set(:borrower_rate, 'test').calculate
    end

    mca_loan_calculator = FlenderCalculator::McaLoan.new(20_000, 120_833.33)
    mca_loan_calculator.set(:lender_rate, 1.15).set(:borrower_rate, 1.2).set(:term, 12 * 20)
    assert_equal mca_loan_calculator.lender_rate, 1.15
    assert_equal mca_loan_calculator.borrower_rate, 1.2
    assert_equal mca_loan_calculator.term, 240

    mca_loan_calculator.calculate
    assert_equal mca_loan_calculator.arrangement_fee, 2_000
    assert_equal mca_loan_calculator.arrangement_fee_percent, 10
    assert_equal mca_loan_calculator.total_sum, 22_000
    assert_equal mca_loan_calculator.factoring_fee, 4_400
    assert_equal mca_loan_calculator.margin_fee.round(2), 1100.0
    assert_equal mca_loan_calculator.margin_rate, 0.05
    assert_equal mca_loan_calculator.lender_return, 23_000
    assert_equal mca_loan_calculator.total_repayment_amount, 27500
    assert_equal mca_loan_calculator.fire_costs, 400
    assert_equal mca_loan_calculator.flender_net_return, 2_700
    assert_equal mca_loan_calculator.flender_net_return_percent, 13.5
    assert_equal mca_loan_calculator.daily_average.round(2), 6_041.67
    assert_equal mca_loan_calculator.weekly_average.round(2), 27_884.61
    assert_equal mca_loan_calculator.monthly_average.round(2), 120_833.33
    assert_equal mca_loan_calculator.annual_average.round(0), 1_450_000
    assert_equal mca_loan_calculator.broker_fee, 0
    assert_equal mca_loan_calculator.broker_fee_percent, 0
    mca_loan_calculator.set(:broker_fee_percent, 15)
    mca_loan_calculator.calculate
    assert_equal mca_loan_calculator.broker_fee, 3000
    assert_equal mca_loan_calculator.broker_fee_percent, 15

    new_arrangement_fees = FlenderCalculator::McaLoan::DEFAULT_ARRANGEMENT_FEE_VALUES.each do |h|
      h[:value] = h[:value] * 2
    end
    mca_loan_calculator.set(:arrangement_fee_values, new_arrangement_fees)
    assert_equal mca_loan_calculator.arrangement_fee_values, new_arrangement_fees
    mca_loan_calculator.calculate
    assert_equal mca_loan_calculator.arrangement_fee, 2_000

    mca_loan_calculator.set(:arrangement_fee, 3_000)
    assert_equal mca_loan_calculator.arrangement_fee, 3_000
    mca_loan_calculator.calculate
    assert_equal mca_loan_calculator.total_sum, 26_000
    assert_equal mca_loan_calculator.factoring_fee, 5_200
    assert_equal mca_loan_calculator.total_repayment_amount, 32500.0

    mca_loan_calculator.set(:lender_rate, 1.5)
    assert_equal mca_loan_calculator.lender_rate, 1.5
    assert_equal mca_loan_calculator.total_repayment_amount, 32500.0
  end

  def test_borrower_rate_zero
    mca_loan_calculator = FlenderCalculator::McaLoan.new(20_000,  1_450_000).set(:borrower_rate, 0.5)
    mca_loan_calculator.calculate
    assert_equal mca_loan_calculator.factoring_fee, 0
  end

  def test_sets_hash
    mca_loan_calculator = FlenderCalculator::McaLoan.new(20_000,  1_450_000).set(:borrower_rate, 0)
    hash = { term: 25, split_level: 25, daily_repayment_amount: 123 }
    mca_loan_calculator.set_hash(hash)
    assert_equal mca_loan_calculator.term, 25
    assert_equal mca_loan_calculator.split_level, 25
    assert_equal mca_loan_calculator.daily_repayment_amount, 123
  end

  def test_margin_fee_calculations
    mca_loan_calculator = FlenderCalculator::McaLoan.new(20_000,  1_450_000)
    mca_loan_calculator.set_hash({ borrower_rate: 1.12, lender_rate: 1.10, arrangement_fee: 4000 })
    assert_equal mca_loan_calculator.borrower_rate, 1.12
    assert_equal mca_loan_calculator.lender_rate, 1.10
    mca_loan_calculator.calculate
    assert_equal mca_loan_calculator.margin_rate, 0.02
    assert_equal mca_loan_calculator.margin_fee, (mca_loan_calculator.amount + mca_loan_calculator.arrangement_fee) * 0.02
    assert_equal mca_loan_calculator.total_repayment_amount, 27360
    mca_loan_calculator.set(:margin_rate, 0.05)
    mca_loan_calculator.calculate
    assert_equal mca_loan_calculator.margin_rate, 0.05
    assert_equal mca_loan_calculator.margin_fee, 24_000 * 0.05
    assert_equal mca_loan_calculator.total_repayment_amount, 28080
  end
end
