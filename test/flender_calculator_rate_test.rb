require "test_helper"

class FlenderCalculatorRateTest < Minitest::Test
  def test_class_load
    refute_nil FlenderCalculator::Rate.calc(12, 12, 10)
  end

  def test_does_not_go_into_infinite_loop
    assert_raises do
      FlenderCalculator::Rate.calc(12, 0, 0.0)
    end
    assert_raises do
      refute_nil FlenderCalculator::Rate.calc(0, 12, 10)
    end
    refute_nil FlenderCalculator::Rate.calc(12, 12, 0)
    refute_nil FlenderCalculator::Rate.calc(12, 0, 10)
  end
end