# FlenderCalculator

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/flender_calculator`. To experiment with that code, run `bin/console` for an interactive prompt.

TODO: Delete this and the text above, and describe your gem

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'flender_calculator'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install flender_calculator

## Usage

TODO: Describe usage for loan and broker calculators

#### MCA calculator:

```ruby
FlenderCalculator::McaLoan.new(amount, turnover)
```

values for term, lender rate, borrower rate and arrangement fees will be defaulted to:
```ruby
BASE_TERM = 1
BASE_BORROWER_RATE = 1.07
BASE_LENDER_RATE = 1.06
DEFAULT_ARRANGEMENT_FEE_VALUES = [
      { amount_from: 0,      amount_to: 20_000,   value: 2000, fire_cost: 400 },
      { amount_from: 20_001, amount_to: 30_000,   value: 2500, fire_cost: 500 },
      { amount_from: 30_001, amount_to: 50_000,   value: 3500, fire_cost: 850 },
      { amount_from: 50_001, amount_to: 75_000,   value: 4500, fire_cost: 1000 },
      { amount_from: 75_001, amount_to: 100_000,  value: 5500, fire_cost: 1200 },
      { amount_from: 100_001, amount_to: 1_000_000,  value: 5500, fire_cost: 2200 }
    ]
```

However these values can be rewritten using `set` function
```ruby
calculator = FlenderCalculator::McaLoan.new(50_000, 12_000_000)
calculator.set(:term, 12).set(:borrower_rate, 1.2).set(:lender_rate, 1.15)
          .set(:arrangement_fee_values, [{ amount_from: 0, amount_to: 1_000_000, value: 3000, fire_cost: 800 }])
```

after  calling `calculator.calculate` function next values would be calculated:
```ruby
[:total_repayment_amount, :factoring_fee, :arrangement_fee, :arrangement_fee_values, :margin_return, :lender_return,
 :borrower_return, :total_sum, :daily_average, :weekly_average, :monthly_average, :margin_fee, :flender_net_return,
 :fire_costs, :daily_repayment_amount, :collections_split, :flender_net_return_percent]
```

* `amount` - net drawdown (amount, that borrower will actually receive)
* `total_sum` - amount that borrower requested with arrangement fee
* `total_repayment_amount` - amount that borrower requested with arrangement fee and factoring fee

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/flender_calculator. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the FlenderCalculator project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/flender_calculator/blob/master/CODE_OF_CONDUCT.md).
